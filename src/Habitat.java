import javax.swing.*;
import java.awt.*;
import java.io.*;
import java.util.*;

public class Habitat {

    private ArrayList<Character> arrBees = new ArrayList<Character>();
    private HashSet<Integer> arrId = new HashSet<Integer>();
    private TreeMap<Integer, Integer> arrBornTime = new TreeMap<Integer, Integer>();
    private int WIDTH = MainProgram.beesWIDTH, HEIGHT = MainProgram.HEIGHT;
    static public int N1 = 3, N2 = 2, K = 40, P = 80, workerLifeTime = 10, droneLifeTime = 10;
    private int imageSize = 50;
    private ImageIcon imageIcon = new ImageIcon(new ImageIcon(getClass().getResource(
            "/Images/sky2.png")).getImage().getScaledInstance(WIDTH,HEIGHT, Image.SCALE_DEFAULT));
    WorkerAI workerAI;
    DroneAI droneAI;

    Habitat(){
        createGUI();
        workerAI = new WorkerAI();
        droneAI = new DroneAI();
        loadConfiguration(MainProgram.fileName);
    }
    synchronized void update(int time){
        removeBees(time);
        workerAI.setArray(arrBees);
        droneAI.setArray(arrBees);
        if(time % N2 == 0)
        {
            int random = (int)Math.floor(Math.random()*100);
            if(random <= P) {
                int x = (int) Math.floor(Math.random() * (WIDTH - imageSize));
                int y = (int) Math.floor(Math.random() * (HEIGHT - imageSize));
                createWorker(x, y, workerLifeTime, time);
            }
        }
        if(time % N1 == 0 && Worker.beeWorker != 0){
            int dronePercent = 100*Drone.beeDrone/(Drone.beeDrone + Worker.beeWorker);
            if(dronePercent < K){
                int x = (int)Math.floor(Math.random()*(WIDTH-imageSize));
                int y = (int)Math.floor(Math.random()*(HEIGHT-imageSize));
                createDrone(x, y, droneLifeTime, time);
            }
        }
    }
    Worker createWorker(int x, int y, int lifeTime, int bornTime){
        Worker worker = new Worker(x, y, lifeTime, bornTime, arrId);
        arrBees.add(worker);
        arrId.add(worker.getId());
        arrBornTime.put(worker.getId(), worker.getBornTime());
        Worker.beeWorker++;
        return worker;
    }
    Drone createDrone(int x, int y, int lifeTime, int bornTime){
        Drone drone = new Drone(x, y, lifeTime, bornTime, arrId);
        arrBees.add(drone);
        arrId.add(drone.getId());
        arrBornTime.put(drone.getId(), drone.getBornTime());
        Drone.beeDrone++;
        return drone;
    }
    void createGUI(){
        JLabel tmp = new JLabel();
        tmp.setBounds( 0, 0, imageIcon.getIconWidth(), imageIcon.getIconHeight());
        tmp.setIcon(imageIcon);
        MainProgram.bees.add(tmp, 0, 0);
        MainProgram.bees.repaint();
    }
    void clear(){
        MainProgram.bees.removeAll();
        arrBees.clear();
        arrId.clear();
        arrBornTime.clear();
        createGUI();
        Worker.beeWorker = Drone.beeDrone = 0;
    }
    void removeBees(int time){
        Iterator<Character> iterator = arrBees.iterator();
        while (iterator.hasNext()){
            Character bee = iterator.next();
            if((time - bee.getBornTime()) >= bee.getLifeTime()){
                MainProgram.bees.remove(bee.getJlb());
                MainProgram.bees.repaint();
                if(bee instanceof Worker){
                    Worker.beeWorker--;
                }
                else{
                    Drone.beeDrone--;
                }
                arrId.remove(bee.getId());
                arrBornTime.remove(bee.getId());
                iterator.remove();
            }
        }

    }
    ArrayList<Character> getArrBees(){return arrBees;}
    void pauseAI(){
        workerAI.setBoolStop(true);
        droneAI.setBoolStop(true);
    }
    void continueAI(){
        workerAI._continue();
        workerAI.setBoolStop(false);
        droneAI._continue();
        droneAI.setBoolStop(false);
    }
    void saveFile(File fileName){
        try {
            FileOutputStream outputStream = new FileOutputStream(fileName);
            ObjectOutputStream objectOutputStream = new ObjectOutputStream(outputStream);
            SavedSimulation savedSimulation = new SavedSimulation(arrBees, arrId, arrBornTime);
            objectOutputStream.writeObject(savedSimulation);
            objectOutputStream.close();
            outputStream.close();
        }catch (IOException e){
            e.getMessage();
        }
    }
    void loadFile(File fileName, int time){
        clear();
        try{
            FileInputStream fileInputStream = new FileInputStream(fileName);
            ObjectInputStream objectInputStream = new ObjectInputStream(fileInputStream);
            SavedSimulation savedSimulation = (SavedSimulation) objectInputStream.readObject();
            arrBees = savedSimulation.arrBees;
            arrId = savedSimulation.arrId;
            objectInputStream.close();
            fileInputStream.close();
        }catch (IOException e){
            e.printStackTrace();
        }catch (ClassNotFoundException e){
            e.printStackTrace();
        }
        for(Character bee : arrBees){
            if(bee instanceof Worker){
                Worker worker = (Worker)bee;
                worker.setBornTime(time);
                arrBornTime.put(worker.getId(), time);
                Worker.beeWorker++;
                MainProgram.bees.add(worker.getJlb(), 0, 0);
            }else{
                Drone drone = (Drone)bee;
                drone.setBornTime(time);
                arrBornTime.put(drone.getId(), time);
                Drone.beeDrone++;
                MainProgram.bees.add(drone.getJlb(), 0, 0);
            }
            MainProgram.bees.repaint();
        }
    }
    void saveConfiguration(String fileName){
        try {
            FileWriter file = new FileWriter(fileName);
            file.write(N1);
            file.write(N2);
            file.write(K);
            file.write(P);
            file.write(workerLifeTime);
            file.write(droneLifeTime);
            file.close();
        } catch (IOException e) {
            e.getMessage();
        }
    }
    void loadConfiguration(String fileName){
        clear();
        try{
            FileReader file = new FileReader(fileName);
            N1 = file.read();
            N2 = file.read();
            K = file.read();
            P = file.read();
            workerLifeTime = file.read();
            droneLifeTime = file.read();
            file.close();
        }catch (IOException e){
            e.getMessage();
        }
    }
    SavedSimulation get_some_bees(int time){
        ArrayList<Character> tmpArr = new ArrayList<>();
        HashSet<Integer> tmpArrId = new HashSet<>();
        TreeMap<Integer, Integer> tmpArrBornTime = new TreeMap<>();
        for(Character bee : arrBees){
            int rand = new Random().nextInt(2);
            if(rand == 1){
                Character worker = bee;
                worker.setLifeTime(worker.getLifeTime() - (time - worker.getBornTime()));
                tmpArr.add(worker);
                tmpArrId.add(worker.getId());
                tmpArrBornTime.put(worker.getId(), worker.getBornTime());
            }
        }
        SavedSimulation savedSimulation = new SavedSimulation(tmpArr, tmpArrId, tmpArrBornTime);

        return savedSimulation;
    }
    void update(ObjectInputStream in, int time){
        try {
            SavedSimulation savedSimulation = (SavedSimulation) in.readObject();
            for(Character bee : savedSimulation.arrBees){
                if(bee instanceof Worker){
                    Worker worker = (Worker)bee;
                    worker.setBornTime(time);
                    arrBees.add(worker);
                    arrId.add(worker.getId());
                    arrBornTime.put(worker.getId(), time);
                    Worker.beeWorker++;
                    MainProgram.bees.add(worker.getJlb(), 0, 0);
                }else{
                    Drone drone = (Drone)bee;
                    drone.setBornTime(time);
                    arrBees.add(drone);
                    arrId.add(drone.getId());
                    arrBornTime.put(drone.getId(), time);
                    Drone.beeDrone++;
                    MainProgram.bees.add(drone.getJlb(), 0, 0);
                }
                MainProgram.bees.repaint();
            }
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }

    }
}
