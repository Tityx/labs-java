import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.TreeMap;

public class SavedSimulation implements Serializable {

    private static final long serialVersionUID = 1L;

    ArrayList<Character> arrBees;
    HashSet<Integer> arrId;
    TreeMap<Integer, Integer> arrBornTime;

    SavedSimulation(ArrayList arrBees, HashSet arrId, TreeMap arrBornTime){
        this.arrBees = arrBees;
        this.arrId = arrId;
        this.arrBornTime = arrBornTime;
    }
}
